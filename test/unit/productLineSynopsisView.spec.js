import ProductLineSynopsisView from '../../src/productLineSynopsisView';
import dummy from '../dummy';

describe('ProductLineSynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ProductLineSynopsisView(null, dummy.productLineName);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new ProductLineSynopsisView(expectedId, dummy.productLineName);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ProductLineSynopsisView(dummy.productLineId, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.productLineName;

            /*
             act
             */
            const objectUnderTest =
                new ProductLineSynopsisView(dummy.productLineId, expectedName);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
        it('does not throw if productGroupId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ProductLineSynopsisView(dummy.productLineId, dummy.productLineName,null);

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });

        it('sets productGroupId', () => {
            /*
             arrange
             */
            const expectedProductGroupId = dummy.productGroupId;

            /*
             act
             */
            const objectUnderTest =
                new ProductLineSynopsisView(dummy.productLineId, dummy.productLineName, expectedProductGroupId);

            /*
             assert
             */
            const actualProductGroupId = objectUnderTest.productGroupId;
            expect(actualProductGroupId).toEqual(expectedProductGroupId);

        });

    })
});
