import ProductLineServiceSdkConfig from './productLineServiceSdkConfig';
import DiContainer from './diContainer';
import ProductLineSynopsisView from './productLineSynopsisView';
import ListProductLinesFeature from './listProductLinesFeature';

/**
 * @class {ProductLineServiceSdk}
 */
export default class ProductLineServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {ProductLineServiceSdkConfig} config
     */
    constructor(config:ProductLineServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    listProductLines(accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListProductLinesFeature)
            .execute(accessToken);

    }

}
